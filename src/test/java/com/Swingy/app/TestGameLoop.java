package com.Swingy.app;

import com.Swingy.app.Model.*;
import com.Swingy.app.View.Console;
import com.Swingy.app.utils.DataBase;
import javafx.util.Pair;
import org.junit.jupiter.api.*;

import com.Swingy.app.Controler.GameLoop;

import com.Swingy.app.View.Interface;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;


public class TestGameLoop {
    private GameLoop ObjGameLoop;

    @BeforeEach
    void initGameloop() {
        DataBase.empty();
        FakeInterface FakeInterface = new FakeInterface();
        this.ObjGameLoop = new GameLoop(FakeInterface);
    }

    @AfterEach
    void emptyGameLoop() {
        DataBase.empty();
    }

    public class FakeInterface implements Interface {
        int i = 0;

        public int numCalled() {return i; }
        public void addCalled() {i++;}

        public String   StartMenu() {return "create"; };
        public Hero.CreateBuilder CreateHero(HeroClass[] classes, String error) {
            Hero.CreateBuilder buildHero = new Hero.CreateBuilder();
            buildHero.WithName(TestModels.CorrectHeroName);
            buildHero.WithClass(classes[0]);
            return buildHero;
        };

        public Pair SelectHero(ArrayList<Hero> heroes) {
            if (heroes.isEmpty())
                return new Pair<Hero, String>(null, "create");
            else
                return new Pair<Hero, String>(heroes.get(0), "create");
        }
        public String    Game(Hero hero, Map map, String log) {
            return log;
        }
        public String    Fight(Hero hero, Villain villain) {
            return "run";
        }
        public String    ChoiceArtifact(Hero hero, Artifact artifact) {return "keep";}
        public void      ResultBattle(Hero hero, Hero old_hero, Villain villain ) {}
        public String    ResultGame(Hero hero, Map map) {return "continue";};
        public void      CloseInterface() {}
    }

    public class FakeSwitch extends FakeInterface {
        @Override
        public String   StartMenu() {
            this.addCalled();
            return "switch";
        }
    }

    public class MockGameLoop extends GameLoop {
        int called = 0;

        public int numCalled() {return called; }
        public void addCalled() {called++;}

        public MockGameLoop(Interface GameInterface) {
            super(GameInterface);
        }

        @Override
        public void CreateHero() {
            this.addCalled();
        }
    }

    @Test
    @DisplayName("test switch interface")
    public void GameLoopSwitch() {
        ObjGameLoop.setGameInterface(new Console());
        String name_interface = ObjGameLoop.getGameInterface().toString();
        assertTrue(name_interface.contains("Console"));
        assertFalse(name_interface.contains("Gui"));
        ObjGameLoop.SwitchInterface();
        name_interface = ObjGameLoop.getGameInterface().toString();
        assertTrue(name_interface.contains("Gui"));
        assertFalse(name_interface.contains("Console"));
        ObjGameLoop.SwitchInterface();
        name_interface = ObjGameLoop.getGameInterface().toString();
        assertTrue(name_interface.contains("Console"));
        assertFalse(name_interface.contains("Gui"));
    }

    @Test
    @DisplayName("test Menu gameloop")
    public void GameLoopMenu() {
        class MockMenugameLoop extends MockGameLoop {
            public int menu_called= 0;
            public MockMenugameLoop(Interface GameInterface) { super(GameInterface); }
            @Override public void CreateHero() { this.addCalled(); }
            @Override public void SelectHero() { this.addCalled(); }
            @Override public void SwitchInterface() { this.addCalled(); }

            @Override
            public void Menu() {
                menu_called +=1;
                if (menu_called < 4) {
                    super.Menu();
                }
            }
        }

        class FakeCreate extends FakeInterface {
            @Override
            public String   StartMenu() {
                this.addCalled();
                return "create";
            }
        }

        class FakeSelect extends FakeInterface {
            @Override
            public String   StartMenu() {
                this.addCalled();
                return "select";
            }
        }

        FakeCreate i_create = new FakeCreate();
        MockMenugameLoop mock = new MockMenugameLoop(i_create);
        mock.Menu();
        assertEquals(1, i_create.numCalled());
        assertEquals(1, mock.numCalled());

        FakeSelect i_select = new FakeSelect();
        mock.setGameInterface(i_select);
        mock.Menu();
        assertEquals(1, i_select.numCalled());
        assertEquals(2, mock.numCalled());

        FakeSwitch i_switch = new FakeSwitch();
        mock.setGameInterface(i_switch);
        mock.Menu();
        assertEquals(1, i_switch.numCalled());
        assertEquals(3, mock.numCalled());
    }

    @Test
    @DisplayName("Test fake interface with create hero")
    public void GameLoopCreate() {
        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();

        HeroClass[] classes = factory.getClasses();
        ObjGameLoop.CreateHero();
        Hero hero = ObjGameLoop.GetHero();
        assertEquals(TestModels.CorrectHeroName, hero.Getname());
        assertEquals(classes[0].GetName(), hero.Get_Class());
    }

    @Test
    @DisplayName("Test create loop validator interface")
    public void GameLoopCreateValidator() {
        class FakeFalseShort extends FakeInterface {
            @Override
            public Hero.CreateBuilder CreateHero(HeroClass[] classes, String error) {
                if (this.i >= 3)
                {
                    return super.CreateHero(classes, error);
                }
                if (error != "") {
                    assertEquals("size must be between 3 and 10,", error);
                }
                Hero.CreateBuilder buildHero = new Hero.CreateBuilder();
                buildHero.WithName(TestModels.longHeroName);
                buildHero.WithClass(classes[0]);
                addCalled();
                return buildHero;
            }
        }
        FakeFalseShort _Interface = new FakeFalseShort();
        ObjGameLoop.setGameInterface(_Interface);
        ObjGameLoop.CreateHero();
        assertEquals(3, _Interface.numCalled());
    }

    @Test
    @DisplayName("Test select hero with fake interface")
    public void GameLoopSelect() {
        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(TestModels.CreateHero("test n1", 0));
        DataBase.empty();
        String hero_name = "test_hero";
        DataBase.insert(hero_name, "Student", 1, 0, 0, 0, 0);
        ObjGameLoop.SelectHero();
        Hero hero = ObjGameLoop.GetHero();
        assertEquals(hero_name, hero.Getname());
    }

    @Test
    @DisplayName("Test start Game")
    public void GameLoopStartGame() {
        ObjGameLoop.SetHero(TestModels.CreateHero("test start", 0));
        Map map = ObjGameLoop.getMap();
        Point position = ObjGameLoop.GetPosition();
        ArrayList<Villain> villains = ObjGameLoop.getVillains();
        assertEquals(null, map);
        assertEquals(null, position);
        assertEquals(null, villains);
        ObjGameLoop.StartGame();

        map = ObjGameLoop.getMap();
        position = ObjGameLoop.GetPosition();
        villains = ObjGameLoop.getVillains();
        assertNotEquals(null, map);
        assertNotEquals(null, position);
        assertNotEquals(null, villains);
        assertEquals(1, ObjGameLoop.getMap().getPosition(ObjGameLoop.GetPosition()));
    }

    @Test
    @DisplayName("Test Game Loop in GameLoop")
    public void GameLoopGame() {
        class FakeCreate extends FakeInterface {
            public int game_called = 0;
            @Override
            public String Game(Hero hero, Map map, String log) {
                if (game_called == 0) {
                    game_called += 1;
                    return "switch";
                }
                if (game_called >= 1) {
                    game_called += 1;
                    return "north_test";
                }
                return "";
            }
        }

        class MockGameLoopGame extends MockGameLoop {
            public int  endGame_called = 0;
            public int  move_called = 0;
            public int  switch_called = 0;
            public int  end_called = 0;
            public MockGameLoopGame(Interface GameInterface) { super(GameInterface); }

            @Override public void EndGame() { endGame_called+=1; }
            @Override public void movePlayer(String direction) { move_called += 1; }
            @Override public void SwitchInterface() { switch_called+=1; }

            @Override
            public boolean checkEndGame() {
                if (end_called == 3)
                    return true;
                end_called +=1;
                return false;
            }
        }

        FakeCreate _mockInterface = new FakeCreate();
        MockGameLoopGame mock = new MockGameLoopGame(_mockInterface);
        mock.SetHero(TestModels.CreateHero("test mover", 0));
        mock.Game();
        assertEquals(3, mock.end_called);
        assertEquals(3, mock.move_called);
        assertEquals(1, mock.switch_called);
        assertEquals(1, mock.endGame_called);
    }

    @Test
    @DisplayName("Test move player")
    public void GameLoopMovePlayer() {
        ObjGameLoop.SetHero(TestModels.CreateHero("test mover", 0));
        ObjGameLoop.setMap(new Map(0));
        ObjGameLoop.SetPosition(ObjGameLoop.getMap().getPositionCenter());
        Point p_1 = ObjGameLoop.GetPosition();
        assertEquals(2, p_1.x);
        assertEquals(2, p_1.y);
        ObjGameLoop.movePlayer("north");
        Point p_2 = ObjGameLoop.GetPosition();
        assertEquals(1, p_2.x);
        assertEquals(2, p_2.y);
        ObjGameLoop.movePlayer("west");
        Point p_3 = ObjGameLoop.GetPosition();
        assertEquals(1, p_3.x);
        assertEquals(1, p_3.y);
        ObjGameLoop.movePlayer("south");
        Point p_4 = ObjGameLoop.GetPosition();
        assertEquals(2, p_4.x);
        assertEquals(1, p_4.y);
        ObjGameLoop.movePlayer("east");
        Point p_5 = ObjGameLoop.GetPosition();
        assertEquals(2, p_5.x);
        assertEquals(2, p_5.y);
    }

    @Test
    @DisplayName("test end game")
    public void GameLoopEndGamme() {
        ObjGameLoop.SetHero(TestModels.CreateHero("test extreme mover", 0));
        ObjGameLoop.setMap(new Map(0));
        ObjGameLoop.SetPosition(ObjGameLoop.getMap().getPositionCenter());
        assertEquals(false, ObjGameLoop.checkEndGame());
        ObjGameLoop.GetHero().setHp(-12);
        assertEquals(true, ObjGameLoop.checkEndGame());
        ObjGameLoop.GetHero().setHp(12);
        assertEquals(false, ObjGameLoop.checkEndGame());
        ObjGameLoop.SetPosition(new Point(-1, -1));
        assertEquals(true, ObjGameLoop.checkEndGame());
    }

    @Test
    @DisplayName("Test move player limits")
    public void GameLoopLimitPlayer() {
        ObjGameLoop.SetHero(TestModels.CreateHero("test extreme mover", 0));
        ObjGameLoop.setMap(new Map(0));
        ObjGameLoop.SetPosition(ObjGameLoop.getMap().getPositionCenter());
        Point p_1 = ObjGameLoop.GetPosition();
        assertEquals(2, p_1.x);
        assertEquals(2, p_1.y);
        assertEquals(true, ObjGameLoop.checkPlayerLimits());
        ObjGameLoop.SetPosition(new Point(0, 2));
        Point p_2 = ObjGameLoop.GetPosition();
        assertEquals(0, p_2.x);
        assertEquals(true, ObjGameLoop.checkPlayerLimits());
        ObjGameLoop.movePlayer("north");
        assertEquals(false, ObjGameLoop.checkPlayerLimits());

        ObjGameLoop.SetPosition(new Point(2, 0));
        Point p_3 = ObjGameLoop.GetPosition();
        assertEquals(0, p_3.y);
        assertEquals(true, ObjGameLoop.checkPlayerLimits());
        ObjGameLoop.movePlayer("west");
        assertEquals(false, ObjGameLoop.checkPlayerLimits());

        ObjGameLoop.SetPosition(new Point(4, 2));
        Point p_4 = ObjGameLoop.GetPosition();
        assertEquals(4, p_4.x);
        assertEquals(true, ObjGameLoop.checkPlayerLimits());
        ObjGameLoop.movePlayer("south");
        assertEquals(false, ObjGameLoop.checkPlayerLimits());

        ObjGameLoop.SetPosition(new Point(2, 4));
        Point p_5 = ObjGameLoop.GetPosition();
        assertEquals(4, p_5.y);
        assertEquals(true, ObjGameLoop.checkPlayerLimits());
        ObjGameLoop.movePlayer("east");
        assertEquals(false, ObjGameLoop.checkPlayerLimits());
    }

    @Test
    @DisplayName("Test level up player")
    public void GameLoopLevelUp() {
        ObjGameLoop.SetHero(TestModels.CreateHero("test levelers", 0));
        assertEquals(0, ObjGameLoop.GetHero().getLevel());
        ObjGameLoop.GetHero().setExperience(1000);
        ObjGameLoop.levelUp();
        assertEquals(1, ObjGameLoop.GetHero().getLevel());
        ObjGameLoop.GetHero().setExperience(2450);
        ObjGameLoop.levelUp();
        assertEquals(2, ObjGameLoop.GetHero().getLevel());
        ObjGameLoop.GetHero().setExperience(4800);
        ObjGameLoop.levelUp();
        assertEquals(3, ObjGameLoop.GetHero().getLevel());
        ObjGameLoop.GetHero().setExperience(8050);
        ObjGameLoop.levelUp();
        assertEquals(4, ObjGameLoop.GetHero().getLevel());
        ObjGameLoop.GetHero().setExperience(12200);
        ObjGameLoop.levelUp();
        assertEquals(5, ObjGameLoop.GetHero().getLevel());

        ObjGameLoop.GetHero().setLevel(0);
        ObjGameLoop.GetHero().setExperience(8050);
        ObjGameLoop.levelUp();
        assertEquals(4, ObjGameLoop.GetHero().getLevel());
    }

    @Test
    @DisplayName("Test Try run")
    public void GameLoopTryRun() {
        Random rand_1 = new Random(1);
        assertEquals(true, ObjGameLoop.TryRun(rand_1));
        Random rand_2 = new Random(2);
        assertEquals(false, ObjGameLoop.TryRun(rand_2));
    }

    public void TestFightHeroVillain(int hero_hp, int hero_attack, int hero_defence,
                                     int villain_hp, int villain_attack, int villain_defence,
                                     boolean expected_result, Random rand) {
        ObjGameLoop.SetHero(TestModels.CreateHero("test FIGHT!", 0));
        ObjGameLoop.GetHero().setHp(hero_hp);
        ObjGameLoop.GetHero().setAttack(hero_attack);
        ObjGameLoop.GetHero().setDefence(hero_defence);
        Villain villain = new Villain();
        villain.setHp(villain_hp);
        villain.setAttack(villain_attack);
        villain.setDefence(villain_defence);
        assertEquals(expected_result, ObjGameLoop.Fight(villain, rand));
    }

    @Test
    @DisplayName("Test Fight")
    public void GameLoopFight() {
        // Win
        TestFightHeroVillain(100, 50, 50,
                70, 20, 30, true, new Random(0));
        TestFightHeroVillain(100, 50, 50,
                90, 20, 30, true, new Random(1));

        // Loose
        TestFightHeroVillain(100, 50, 50,
                70, 20, 70, false, new Random(0));
        TestFightHeroVillain(100, 50, 30,
                120, 50, 40, false, new Random(0));
    }
}

package com.Swingy.app;

import com.Swingy.app.TestModels;
import com.Swingy.app.utils.DataBase;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.Swingy.app.Model.Hero;

import java.util.ArrayList;
import com.Swingy.app.utils.DataBase;

public class TestDatabase {
    @BeforeEach
    void emptyDatabase(TestInfo testInfo){
        DataBase.empty();
    }

    @BeforeAll
    static void CreateDatabase() {
        //DataBase.createDatabase();
        DataBase.initDatabase();
    }

    @AfterAll
    static void EmptyDatabase() {
        DataBase.empty();
    }

    @Test
    @DisplayName("Test insert hero in database")
    public void SaveHero() {
        String Name = "test";
        String NameClass = "Paladin";
        int level = 1;
        int xp = 0;
        int attack = 0;
        int defence = 0;
        int hp = 1;
        int id = 0;

        id = DataBase.insert(Name, NameClass, level, xp, attack, defence, hp);

        Hero h = DataBase.getHeroById(id);
        assertEquals(Name, h.Getname());
    }

    @Test
    @DisplayName("Test get hero in database")
    public void GetHero() {
        Hero hero = TestModels.CreateHero("julian", 0);
        int id = DataBase.insert(hero.Getname(), hero.Get_Class(), hero.getLevel(), hero.getExperience(), hero.getAttack(), hero.getDefence(), hero.getHp());
        hero.setId(id);

        Hero new_hero = DataBase.getHeroById(hero.getId());
        assertEquals(hero.Getname(), new_hero.Getname());
        assertEquals(hero.getLevel(), new_hero.getLevel());
        assertEquals(hero.getExperience(), new_hero.getExperience());
        assertEquals(hero.getAttack(), new_hero.getAttack());
        assertEquals(hero.getDefence(), new_hero.getDefence());
        assertEquals(hero.getHp(), new_hero.getHp());
    }

    @Test
    @DisplayName("Test update hero in Database")
    public void UpdateHero() {
        Hero hero = TestModels.CreateHero("julian", 0);
        int id = DataBase.insert(hero.Getname(), hero.Get_Class(), hero.getLevel(), hero.getExperience(), hero.getAttack(), hero.getDefence(), hero.getHp());
        hero.setId(id);

        hero.SetName("TOTO!");
        hero.AddHp(50);
        hero.AddExperience(1012);
        hero.AddAttack(23);
        hero.AddDefence(12);
        hero.AddLevel(2);

        DataBase.updateHero(hero);

        Hero new_hero = DataBase.getHeroById(hero.getId());

        assertEquals("TOTO!", new_hero.Getname());
        assertEquals(140, new_hero.getHp());
        assertEquals(1012, new_hero.getExperience());
        assertEquals(73, new_hero.getAttack());
        assertEquals(62, new_hero.getDefence());
        assertEquals(2, new_hero.getLevel());
    }

    @Test
    @DisplayName("Test get multiples heroes")
    public void GetAllHero() {
        String name_tab[] = {"Hero 1 t", "Hero 2 t"};
        String class_tab[] = {"Paladin", "Warrior"};
        int level_1 = 1, level_2 = 3;
        int xp_1 = 1, xp_2 = 3;
        int attack_1 = 1, attack_2 = 3;
        int defence_1 = 1, defence_2 = 3;
        int hp_1 = 1, hp_2 = 3;

        int id_1 = DataBase.insert(name_tab[0], class_tab[0], level_1, xp_1, attack_1, defence_1, hp_1);
        int id_2 = DataBase.insert(name_tab[1], class_tab[1], level_2, xp_2, attack_2, defence_2, hp_2);

        ArrayList<com.Swingy.app.Model.Hero> heroes = DataBase.getAllHeroes();

        for (int i = 0; i < heroes.size() ; i++)
        {
            assertEquals(heroes.get(i).Getname(), name_tab[i]);
            System.out.println("hero name:" + heroes.get(i).Getname() + name_tab[i]);
        }
    }
}

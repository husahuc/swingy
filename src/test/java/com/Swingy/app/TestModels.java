package com.Swingy.app;

import com.Swingy.app.Model.*;
import org.junit.jupiter.api.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.jupiter.api.Assertions.*;

public class TestModels {
    public static final String CorrectHeroName = "Neron";
    public static final String longHeroName = "Nonononononononononononononononononononononon";
    public static final String shortHeroName = "Ne";

    static public Hero CreateHero(String HeroName, int class_id) {
        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();

        HeroClass[] classes = factory.getClasses();
        Hero.CreateBuilder build_Hero = new Hero.CreateBuilder().WithName(HeroName).WithClass(classes[class_id]);
        Hero HeroObj = build_Hero.build();
        return HeroObj;
    }

    @Test
    @DisplayName("Test builder of hero correct")
    public void builderHero() {
        String HeroName = "TestHeroPerson";
        Hero HeroObj = CreateHero(HeroName, 0);

        assertEquals(HeroName, HeroObj.Getname());
    }

    @Test
    @DisplayName("Test builder of hero correct")
    public void HeroIsAlive() {
        String HeroName = "TestHeroPerson";
        Hero HeroObj = CreateHero(HeroName, 0);

        assertEquals(true, HeroObj.IsAlive());
        HeroObj.setHp(0);
        assertEquals(false, HeroObj.IsAlive());
        HeroObj.setHp(-1);
        assertEquals(false, HeroObj.IsAlive());
    }

    @Test
    @DisplayName("Test builder of hero name exception")
    public void builderHeroValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Hero.CreateBuilder build_Hero = new Hero.CreateBuilder();
        Set<ConstraintViolation<Hero.CreateBuilder>> constraintViolations_1 = validator.validate( build_Hero );
        assertEquals( 1, constraintViolations_1.size() );
        assertEquals("may not be null", constraintViolations_1.iterator().next().getMessage());

        build_Hero.WithName(shortHeroName);
        Set<ConstraintViolation<Hero.CreateBuilder>> constraintViolations_2 = validator.validate( build_Hero );
        assertEquals( 1, constraintViolations_2.size() );
        assertEquals("size must be between 3 and 10", constraintViolations_2.iterator().next().getMessage());


        build_Hero.WithName(longHeroName);
        Set<ConstraintViolation<Hero.CreateBuilder>> constraintViolations_3 = validator.validate( build_Hero );
        assertEquals( 1, constraintViolations_3.size() );
        assertEquals("size must be between 3 and 10", constraintViolations_3.iterator().next().getMessage());

        build_Hero.WithName(CorrectHeroName);
        Set<ConstraintViolation<Hero.CreateBuilder>> constraintViolations_4 = validator.validate( build_Hero );
        assertEquals( 0, constraintViolations_4.size() );
    }

    @Test
    @DisplayName("Test classes Factory and toString")
    public void FactoryClasses() {
        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();

        HeroClass[] classes = factory.getClasses();
        assertEquals("Paladin", classes[0].GetName());
        assertEquals(50, classes[0].GetAttack());
        assertEquals(90, classes[0].GetHp());
        assertEquals(50, classes[0].GetDefence());
        assertEquals("'" + classes[0].GetName() + '\'' +
                ", attack=" + classes[0].GetAttack() +
                ", defence=" + classes[0].GetDefence() +
                ", hp=" + classes[0].GetHp(), classes[0].toString());
    }

    @Test
    @DisplayName("Map creation with different level")
    public void MapSize() {
        Map map_1 = new Map(1);
        assertEquals(9, map_1.getMap().length);
        Map map_2 = new Map(2);
        assertEquals(15, map_2.getMap().length);
        Map map_5 = new Map(5);
        assertEquals(29, map_5.getMap().length);
        Map map_7 = new Map(7);
        assertEquals(39, map_7.getMap().length);
    }

    @Test
    @DisplayName("Map get center Point with different level")
    public void MapCenter() {
        Map map_1 = new Map(1);
        Point p_1 = map_1.getPositionCenter();
        assertEquals(4, p_1.x);
        Map map_2 = new Map(2);
        Point p_2 = map_2.getPositionCenter();
        assertEquals(7, p_2.x);
        Map map_5 = new Map(5);
        Point p_5 = map_5.getPositionCenter();
        assertEquals(14, p_5.x);
        Map map_7 = new Map(7);
        Point p_7 = map_7.getPositionCenter();
        assertEquals(19, p_7.x);
    }

    @Test
    @DisplayName("Map populate villain")
    public void MapPopulateVillain() {
        int level = 3;
        Map map_1 = new Map(level);
        Random rand = new Random(2);
        ArrayList<Villain> villains = map_1.PopulateVillain(level, rand);
        assertEquals(23, villains.size());

        for (Villain v : villains) {
            Point p = new Point(v.getPosition().x, v.getPosition().y);
            assertEquals(2, map_1.getPosition(p));
        }
    }

    @Test
    @DisplayName("Villain generator")
    public void TestGetVillainInArray() {
        int level = 2;
        Map map_1 = new Map(level);
        Random rand = new Random(3);
        ArrayList<Villain> villains = map_1.PopulateVillain(level, rand);

        for (Villain v : villains) {
            Villain v_test = Villain.GetVillainInArray(v.getPosition(), villains);
            assertTrue(v.equals(v_test));
        }

        Villain v_false = Villain.GetVillainInArray(new Point(0, 0), villains);
        assertEquals(null, v_false);
    }

    @Test
    @DisplayName("Villain generator")
    public void VillainGenerator() {
        Villain.VillainFactory factory = new Villain.VillainFactory();
        Random rand = new Random(1);
        Villain v1 = factory.getVillain(2, rand);
        assertEquals(14, v1.getAttack());
        assertEquals(6, v1.getDefence());
        assertEquals(77, v1.getHp());

        Villain v2 = factory.getVillain(5, rand);
        assertEquals(15, v2.getAttack());
        assertEquals(12, v2.getDefence());
        assertEquals(64, v2.getHp());
    }

    @Test
    @DisplayName("Artifact generator")
    public void ArtifactGenerator() {
        Villain villain = new Villain();
        villain.setAttack(28);
        villain.setDefence(68);
        villain.setHp(100);

        Artifact.ArtifactFatory factory = new Artifact.ArtifactFatory();

        Random rand = new Random(1);
        Artifact a_1 = factory.getArtifact(villain, rand);
        assertEquals("Weapon", a_1.getType());
        assertEquals(28, a_1.getValue());

        rand.setSeed(2);
        Artifact a_2 = factory.getArtifact(villain, rand);
        assertEquals("Armor", a_2.getType());
        assertEquals(68, a_2.getValue());

        rand.setSeed(3);
        Artifact a_3 = factory.getArtifact(villain, rand);
        assertEquals("Helm", a_3.getType());
        assertEquals(100, a_3.getValue());
    }
}

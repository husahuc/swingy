package com.Swingy.app;

import com.Swingy.app.Model.*;
import com.Swingy.app.View.Console;
import com.Swingy.app.View.Interface;
import javafx.util.Pair;
import org.junit.jupiter.api.*;

import com.Swingy.app.Controler.GameLoop;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestConsoleInterface {
    private ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private PrintStream originalOut = System.out;
    private PrintStream originalErr = System.err;
    private InputStream originalIn = System.in;
    private ByteArrayInputStream testIn;

    private Console ConsoleInterface = new Console();

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach()
    public void endStream() {
        this.ConsoleInterface.CloseInterface();
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        InputStream inWithoutClose = new FilterInputStream(new ByteArrayInputStream(data.getBytes())) {
            @Override
            public void close() throws IOException {
            }

        };
        System.setIn(inWithoutClose);
    }

    @AfterEach
    public void restoreStreams() {
        System.setIn(originalIn);
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    @DisplayName("Test the menu")
    public void TestConsoleMenu() {

        provideInput("create\n");

        String result = ConsoleInterface.StartMenu();
        String output = outContent.toString();
        assertTrue(output.contains("create"));
        assertTrue(output.contains("select"));
        assertTrue(output.contains("switch"));
        assertEquals(result, "create");
    }

    @Test
    @DisplayName("Create hero")
    public void TestConsoleCreateHero() {
        provideInput("test_personn\nPaladin\n");

        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();

        HeroClass[] classes = factory.getClasses();
        Hero.CreateBuilder hero =  ConsoleInterface.CreateHero(classes, "test");
        String output = outContent.toString();
        assertTrue(output.contains("name:"));
        assertTrue(output.contains("Paladin"));
        assertEquals(classes[0].GetName(), hero.get_Class().GetName());
        assertEquals("test_personn", hero.getName());
    }

    @Test
    @DisplayName("Test select hero")
    public void TestConsoleSelectHero()
    {
        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(TestModels.CreateHero("test n1", 0));
        heroes.add(TestModels.CreateHero("test n2", 1));
        heroes.get(1).SetId(4);

        provideInput("1\n");

        Pair<Hero, String> pair = this.ConsoleInterface.SelectHero(heroes);
        Hero hero = pair.getKey();
        String output = outContent.toString();
        assertTrue(output.contains(heroes.get(0).Getname()));
        assertTrue(output.contains(heroes.get(1).Getname()));
        assertEquals(heroes.get(1), hero);
        assertEquals("1", pair.getValue());
    }

    @Test
    @DisplayName("Test select hero empty")
    public void TestConsoleSelectHeroEmpty()
    {
        Hero hero;
        ArrayList<Hero> heroes = new ArrayList<>();

        provideInput("o\n");

        Pair<Hero, String> pair = this.ConsoleInterface.SelectHero(heroes);
        hero = pair.getKey();
        String output = outContent.toString();
        assertEquals(null, hero);
        assertEquals("create", pair.getValue());
    }

    @Test
    @DisplayName("Test select hero create")
    public void TestConsoleSelectHeroCreate()
    {
        Hero hero;
        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(TestModels.CreateHero("test n1", 0));

        provideInput("create\n");

        Pair<Hero, String> pair = this.ConsoleInterface.SelectHero(heroes);
        hero = pair.getKey();
        String output = outContent.toString();
        assertEquals(null, hero);
        assertEquals("create", pair.getValue());
    }

    @Test
    @DisplayName("Test select hero too high")
    public void TestConsoleSelectHeroTooHigh()
    {
        Hero hero;
        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(TestModels.CreateHero("test n1", 0));
        heroes.add(TestModels.CreateHero("test n2", 1));

        provideInput("5\n0\n");

        Pair<Hero, String> pair = this.ConsoleInterface.SelectHero(heroes);
        hero = pair.getKey();
        String output = outContent.toString();
        assertEquals(heroes.get(0), hero);
    }

    @Test
    @DisplayName("Test Fight options or run")
    public void TestConsoleFight()
    {
        Hero hero = TestModels.CreateHero("test ", 0);
        Villain.VillainFactory factory = new Villain.VillainFactory();
        Random rand = new Random(1);
        Villain villain = factory.getVillain(2, rand);
        provideInput("fight");

        String choice_1 = this.ConsoleInterface.Fight(hero, villain);
        assertEquals("fight", choice_1);
    }

    @Test
    @DisplayName("test print map")
    public void testPrintMap() {
        Map map = new Map(1);
        map.setPosition(map.getPositionCenter(), 1);
        map.PopulateVillain(1, new Random(1));
        map.setPosition(new Point(0, 0), 3);

        // empty map
        this.ConsoleInterface.PrintMap(map, false);
        String output_1 = outContent.toString();
        assertTrue(output_1.contains(" "));
        assertTrue(output_1.contains("x"));
        assertFalse(output_1.contains("o"));
        assertFalse(output_1.contains("·"));

        System.setOut(new PrintStream(outContent));
        this.ConsoleInterface.PrintMap(map, true);
        String output_2 = outContent.toString();
        assertTrue(output_2.contains(" "));
        assertTrue(output_2.contains("x"));
        assertTrue(output_2.contains("o"));
        assertTrue(output_2.contains("·"));
    }

    @Test
    @DisplayName("test game interface directions")
    public void testGame() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Map map = new Map(hero.getLevel());

        provideInput("north\nsouth\neast\nwest\nswitch\n");
        String choice_1 = this.ConsoleInterface.Game(hero, map, "");
        assertEquals("north", choice_1);

        String choice_2 = this.ConsoleInterface.Game(hero, map, "");
        assertEquals("south", choice_2);

        String choice_3 = this.ConsoleInterface.Game(hero, map, "");
        assertEquals("east", choice_3);

        String choice_4 = this.ConsoleInterface.Game(hero, map, "");
        assertEquals("west", choice_4);

        String choice_5 = this.ConsoleInterface.Game(hero, map, "");
        assertEquals("switch", choice_5);
    }

    @Test
    @DisplayName("test choice artifact")
    public void testChoiceArtifact() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Artifact artifact_w = new Artifact("Weapon", 50);
        Artifact artifact_a = new Artifact("Armor", 25);
        Artifact artifact_h = new Artifact("Helm", 12);

        provideInput("leave\nkeep\nswitch\n");
        String choice_1 = this.ConsoleInterface.ChoiceArtifact(hero, artifact_w);
        String output_1 = outContent.toString();
        assertTrue(output_1.contains("Weapon"));
        assertTrue(output_1.contains("50"));
        assertEquals("leave", choice_1);

        String choice_2 = this.ConsoleInterface.ChoiceArtifact(hero, artifact_a);
        String output_2 = outContent.toString();
        assertTrue(output_2.contains("Armor"));
        assertTrue(output_2.contains("25"));
        assertEquals("keep", choice_2);

        String choice_3 = this.ConsoleInterface.ChoiceArtifact(hero, artifact_h);
        String output_3 = outContent.toString();
        assertTrue(output_3.contains("Helm"));
        assertTrue(output_3.contains("12"));
        assertEquals("switch", choice_3);
    }

    @Test
    @DisplayName("test result battle")
    public void testResultBattle() {
        Villain.VillainFactory factory = new Villain.VillainFactory();
        Random rand = new Random(1);
        Villain villain = factory.getVillain(2, rand);

        Hero old_hero = TestModels.CreateHero("test ", 0);
        Hero hero = (Hero) old_hero.clone();
        hero.setAttack(92);
        hero.setDefence(98);
        hero.setExperience(1001);
        hero.setHp(101);
        hero.setLevel(1);
        this.ConsoleInterface.ResultBattle(hero, old_hero, villain);
        String output_1 = outContent.toString();
        assertTrue(output_1.contains("You have 1 new level"));
        assertTrue(output_1.contains("won"));

        hero.setHp(-19);
        this.ConsoleInterface.ResultBattle(hero, old_hero, villain);
        String output_2 = outContent.toString();
        assertTrue(output_2.contains("lost"));
    }

    @Test
    @DisplayName("test result Game")
    public void testResultGame() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Map map = new Map(hero.getLevel());

        provideInput("continue\nquit\nswitch\n");
        String choice_1 = this.ConsoleInterface.ResultGame(hero, map);
        assertEquals("continue", choice_1);

        String choice_2 = this.ConsoleInterface.ResultGame(hero, map);
        assertEquals("quit", choice_2);

        String choice_3 = this.ConsoleInterface.ResultGame(hero, map);
        assertEquals("switch", choice_3);
    }
}

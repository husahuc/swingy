package com.Swingy.app;

import com.Swingy.app.Model.*;
import com.Swingy.app.View.Gui;
import com.Swingy.app.View.Interface;
import javafx.util.Pair;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestGuiInterface {
    private Interface GuiInterface = new Gui();

    @Test
    @Disabled
    @DisplayName("Test the menu")
    public void testMenu() {
        String choice = GuiInterface.StartMenu();
        assertEquals("select", choice);
    }

    @Test
    @Disabled
    @DisplayName("Test the create hero")
    public void testCreate() {
        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();
        HeroClass[] classes = factory.getClasses();

        Hero.CreateBuilder hero = GuiInterface.CreateHero(classes, "test");
        assertEquals(classes[0].GetName(), hero.get_Class().GetName());
        assertEquals("test", hero.getName());
    }

    @Test
    @Disabled
    @DisplayName("Test the select hero")
    public void testSelect() {
        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(TestModels.CreateHero("test n1", 0));
        heroes.add(TestModels.CreateHero("test n2", 1));
        heroes.get(1).SetId(4);
        Pair<Hero, String> pair = this.GuiInterface.SelectHero(heroes);
        Hero hero = pair.getKey();

        assertEquals(heroes.get(0), hero);
    }

    @Test
    @Disabled
    @DisplayName("Test the game")
    public void testGame() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Map map = new Map(6);
        map.setPosition(map.getPositionCenter(), 1);
        map.PopulateVillain(hero.getLevel(), new Random());

        String choice = this.GuiInterface.Game(hero, map, "");
        assertEquals("north", choice);
    }

    @Test
    @Disabled
    @DisplayName("Test the fight hero")
    public void testFight() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Villain.VillainFactory factory = new Villain.VillainFactory();
        Random rand = new Random(1);
        Villain villain = factory.getVillain(2, rand);

        String choice = this.GuiInterface.Fight(hero, villain);
        assertEquals("Fight", choice);
    }

    @Test
    @Disabled
    @DisplayName("Test gain artifact")
    public void testArtifact() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Artifact.ArtifactFatory factory = new Artifact.ArtifactFatory();
        Villain villain = new Villain();
        villain.setAttack(28);
        villain.setDefence(68);
        villain.setHp(100);
        Random rand = new Random(1);
        Artifact artifact = factory.getArtifact(villain, rand);

        String choice = this.GuiInterface.ChoiceArtifact(hero, artifact);
        assertEquals("keep", choice);
    }

    @Test
    @Disabled
    @DisplayName("Test Result fight")
    public void testResultFight() {
        Villain.VillainFactory factory = new Villain.VillainFactory();
        Random rand = new Random(1);
        Villain villain = factory.getVillain(2, rand);

        Hero old_hero = TestModels.CreateHero("test ", 0);
        Hero hero = (Hero) old_hero.clone();
        hero.setAttack(92);
        hero.setDefence(98);
        hero.setExperience(1001);
        hero.setHp(101);
        hero.setLevel(1);
        this.GuiInterface.ResultBattle(hero, old_hero, villain);
    }

    @Test
    @Disabled
    @DisplayName("Test end result game")
    public void testResultGame() {
        Hero hero = TestModels.CreateHero("test ", 0);
        Map map = new Map(2);
        map.setPosition(map.getPositionCenter(), 1);
        map.PopulateVillain(hero.getLevel(), new Random());

        String choice = this.GuiInterface.ResultGame(hero, map);
        assertEquals("continue", choice);
    }
}

package com.Swingy.app.Controler;
import com.Swingy.app.Model.*;
import com.Swingy.app.View.Console;
import com.Swingy.app.View.Gui;
import com.Swingy.app.View.Interface;
import com.Swingy.app.utils.DataBase;
import javafx.util.Pair;

import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class GameLoop {
    Interface GameInterface;
    Hero hero = null;
    Map map;
    Point position;
    ArrayList<Villain> villains;
    private Random rand = new Random();

    public GameLoop(Interface GameInterface) {
        this.GameInterface = GameInterface;
    }

    public void SwitchInterface() {
        this.GameInterface.CloseInterface();
        if (this.GameInterface instanceof Gui) {
            this.GameInterface.CloseInterface();
            this.GameInterface = new Console();
        }
        else if (this.GameInterface instanceof Console) {
            this.GameInterface.CloseInterface();
            this.GameInterface = new Gui();
        }
    }

    public void Start() {
        DataBase.initDatabase();
        this.Menu();
        if (this.hero != null)
        {
            this.StartGame();
            this.Game();
        }
    }

    public void   Menu() {
        String choice = this.GameInterface.StartMenu();

        if (choice.equals("create")) {
            this.CreateHero();
        }
        if (choice.equals("select")) {
            this.SelectHero();
        }
        if (choice.equals("switch")) {
            SwitchInterface();
            this.Menu();
        }
    }

    public void SelectHero() {
        Hero hero = null;
        ArrayList<Hero> heroes = DataBase.getAllHeroes();

        Pair<Hero, String> pair = this.GameInterface.SelectHero(heroes);
        hero = pair.getKey();
        if (hero != null)
            this.hero = hero;
        else if (pair.getValue().equals("create"))
            this.CreateHero();
        else if (pair.getValue().equals("switch"))
        {
            SwitchInterface();
            this.SelectHero();
        }
    }

    public void CreateHero() {
        System.out.println("2");

        ValidatorFactory validatorfactory = Validation.buildDefaultValidatorFactory();
        System.out.println("1");
        Validator validator = validatorfactory.getValidator();

        HeroClass.HeroClassFactory factory = new HeroClass.HeroClassFactory();

        HeroClass[] classes = factory.getClasses();
        String error = "";
        Hero.CreateBuilder build_hero;

        while (true) {
            build_hero = this.GameInterface.CreateHero(classes, error);
            Set<ConstraintViolation<Hero.CreateBuilder>> constraintViolations = validator.validate(build_hero);
            if (constraintViolations.size() == 0)
                break;
            error = "";
            for (ConstraintViolation<Hero.CreateBuilder> violation : constraintViolations) {
                error += violation.getMessage() + ",";
            }
        }
        this.hero = build_hero.build();
        int id = DataBase.insert(this.hero.Getname(), this.hero.Get_Class(), this.hero.getLevel(), this.hero.getExperience(),
                this.hero.getAttack(), this.hero.getDefence(), this.hero.getHp());
        this.hero.setId(id);
    }

    public void StartGame() {
        this.map = new Map(this.hero.getLevel());
        this.position = this.map.getPositionCenter();
        this.villains = this.map.PopulateVillain(this.hero.getLevel(), this.rand);
        this.map.setPosition(this.position, 1);
    }

    public void  Game() {
        String choice;
        while (true) {
            choice = this.GameInterface.Game(this.hero, this.map, "");
            if (choice.equals("switch"))
                this.SwitchInterface();
            else
                this.movePlayer(choice);
            if (this.checkEndGame())
                break;
            DataBase.updateHero(this.hero);
        }
        this.EndGame();
    }

    public void movePlayer(String direction) {
        Point old_position = (Point) this.position.clone();

        Point new_position = (Point) old_position.clone();
        if (direction.equals("north"))
            new_position.x = old_position.x - 1;
        else if (direction.equals(("south")))
            new_position.x = old_position.x + 1;
        else if (direction.equals(("east")))
            new_position.y = old_position.y + 1;
        else if (direction.equals(("west")))
            new_position.y = old_position.y - 1;

        if (map.getPosition(new_position) == 2) {
            if (this.FightVillain(new_position) < 0)
                new_position = old_position;
        }
        map.setPosition(old_position, 3);
        map.setPosition(new_position, 1);
        this.position = new_position;
    }

    public boolean checkEndGame() {
        if (! this.checkPlayerLimits())
            return true;
        if (this.hero.getHp() < 0)
            return true;
        return false;
    }

    public boolean checkPlayerLimits() {
        if (this.position.x < 0 || this.position.y < 0 ||
            this.position.x > this.map.getLenght() ||
            this.position.y > this.map.getLenght())
            return false;
        else
            return true;
    }

    public void levelUp() {
        while (true) {
            int next_level = this.hero.getLevel() + 1;
            int xp = this.hero.getExperience();
            int level_xp = (int) (next_level * 1000 + Math.pow(next_level -1, 2) * 450);
            if (xp < level_xp)
                break;
            this.hero.AddLevel(1);
        }
    }

    public int FightVillain(Point position) {
        Villain villain = Villain.GetVillainInArray(position, this.villains);
        String choice = "switch";
        while (choice.contains("switch")) {
            choice = this.GameInterface.Fight(this.hero, villain);
            if (choice.contains("switch")) {
                this.SwitchInterface();
            }
        }
        if (choice.contains("run"))
        {
            if (TryRun(rand))
                return -1;
            else
                choice = "fight";
        }
        if (choice.equals("fight")) {
            Hero old_hero = this.hero;
            if (this.Fight(villain, rand)) {
                this.GetHero().AddExperience(villain.getAttack() + villain.getDefence());
                this.levelUp();
                ArtifactGain(villain, rand);
            }
            this.GameInterface.ResultBattle(this.hero, old_hero, villain);
            return (1);
        }
        return (-2);
    }

    public boolean TryRun(Random rand) {
        int chance = rand.nextInt(100);
        if (chance < 50)
            return false;
        else
            return true;
    }

    public boolean Fight(Villain villain, Random rand) {
        int hero_tmp_hp = this.hero.getHp();
        while (villain.getHp() > 0) {
            int damage_villain = this.hero.getAttack() - villain.getDefence();
            if (damage_villain < 0)
                return false;
            if (rand.nextInt(100) <= 25)
                damage_villain *= 2;
            int damage_hero = villain.getAttack() - this.hero.getDefence();
            villain.removeHp(damage_villain);
            hero_tmp_hp -= damage_hero;
            if (hero_tmp_hp < 0)
                return false;
        }
        return true;
    }

    public void ArtifactGain(Villain villain, Random rand) {
        int chance = rand.nextInt(100);
        if (chance < 25)
            return;

        Artifact.ArtifactFatory factory = new Artifact.ArtifactFatory();
        Artifact artifact = factory.getArtifact(villain, rand);
        String choice = "switch";
        while (choice.contains("switch")) {
            choice = this.GameInterface.ChoiceArtifact(this.hero, artifact);
            if (choice.contains("switch")) {
                this.SwitchInterface();
            }
        }
        if (choice.contains("keep"))
            this.hero.ApplyArtifact(artifact);
    }

    public void EndGame() {
        String choice = "switch";
        while (choice.contains("switch")) {
            choice = this.GameInterface.ResultGame(this.hero, this.map);
            if (choice.contains("switch")) {
                this.SwitchInterface();
            }
        }
        this.hero.AddExperience((this.hero.getLevel() +1) * 1000);
        this.levelUp();
        DataBase.updateHero(this.hero);
        if (choice.equals("continue")) {
            this.StartGame();
            this.Game();
        }
        if (choice.equals("quit"))
            this.GameInterface.CloseInterface();
    }

    //getters
    public Hero GetHero() {return this.hero;}
    public Point GetPosition() {return this.position;}
    public Map getMap() {return map;}
    public Interface getGameInterface() { return GameInterface; }
    public ArrayList<Villain> getVillains() { return villains; }

    //setters
    public void SetHero(Hero hero) { this.hero = hero; }
    public void SetPosition(Point p) { this.position = p; }
    public void setMap(Map map) {this.map = map;}

    public void setGameInterface(Interface gameInterface) { GameInterface = gameInterface; }
}

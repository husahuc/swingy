package com.Swingy.app.utils;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.HeroClass;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {
    private static final String DB_URL = "jdbc:sqlite:heroes.db";
    private static Connection connection;

    private static void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(DB_URL);
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("Error connect DB:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void disconnect(Statement statement) {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            System.err.println("Error DB:" + e.getMessage());
        }
    }

    private static Connection getConnection() {
        if (connection == null) {
            connect();
        }
        return connection;
    }

    public static void initDatabase() {
        String sql_create_table = "CREATE TABLE IF NOT EXISTS heroes " +
                "(id INTEGER not NULL, " +
                " name VARCHAR(255)," +
                " class_name VARCHAR(255)," +
                " level INTEGER," +
                " xp INTEGER," +
                " attack INTEGER," +
                " defense INTEGER," +
                " hp INTEGER," +
                " PRIMARY KEY ( id ))";
        try {
            Statement statement = getConnection().createStatement();
            statement.executeUpdate(sql_create_table);
            disconnect(statement);
        } catch (SQLException e) {
            System.err.println("Error DB init:" + e.getMessage());
        }
    }

    public static void empty() {
        String sql_empty = "DELETE FROM heroes;";
        try {
            Statement stmt = getConnection().createStatement();
            stmt.executeUpdate(sql_empty);
            disconnect(stmt);
        } catch (SQLException e) {
            System.err.println("Error DB:" + e.getMessage());
        }
    }

    public static int insert(String name, String class_name, int level, int xp, int attack, int defense, int hp) {
        String sqlQuery = "INSERT INTO heroes(name, class_name, level, xp, attack, defense, hp) VALUES(?, ?, ?, ?, ?, ?, ?)";
        String generatedColumns[] = { "ID" };

        try {
            PreparedStatement p_statement = getConnection().prepareStatement(sqlQuery, generatedColumns);
            p_statement.setString(1, name);
            p_statement.setString(2, class_name);
            p_statement.setInt(3, level);
            p_statement.setInt(4, xp);
            p_statement.setInt(5, attack);
            p_statement.setInt(6, defense);
            p_statement.setInt(7, hp);
            p_statement.executeUpdate();

            ResultSet rs = p_statement.getGeneratedKeys();

            if (rs.next() ) {
                int id = rs.getInt(1);
                return (id);
            }

        } catch (SQLException e) {
            System.err.println("Error DB 3:" + e.getMessage());
            return (-2);
        }
        return (-1);
    }

    private static Hero makeHero(ResultSet rs) {
        try {
            Hero.DatabaseBuilder builder = new Hero.DatabaseBuilder(
                    rs.getString("name"),
                    rs.getString("class_name"),
                    rs.getInt("level"),
                    rs.getInt("xp"),
                    rs.getInt("attack"),
                    rs.getInt("defense"),
                    rs.getInt("hp"));
            Hero hero = builder.build();

            return (hero);
        } catch (SQLException e) {
            System.err.println("Error DB:" + e.getMessage());
        }
        return null;
    }

    public static Hero getHeroById(int id) {
        String sqlQuery = "SELECT * FROM heroes WHERE id = ?";

        try (PreparedStatement p_statement = getConnection().prepareStatement(sqlQuery)) {
            p_statement.setInt(1, id);
            ResultSet rs = p_statement.executeQuery();
            if (rs.next()) {
                return makeHero(rs);
            }
        } catch (SQLException e) {
            System.out.println("Error DB:" + e.getMessage());
        }
        return null;
    }

    public static void updateHero(Hero hero) {
        String sqlQuery = "UPDATE heroes SET name = ?, class_name = ?, level = ?, xp = ?, attack = ?, defense = ?, hp = ? WHERE id = ?";

        try (PreparedStatement p_statement = getConnection().prepareStatement(sqlQuery)) {
            p_statement.setString(1, hero.Getname());
            p_statement.setString(2, hero.Get_Class());
            p_statement.setInt(3, hero.getLevel());
            p_statement.setInt(4, hero.getExperience());
            p_statement.setInt(5, hero.getAttack());
            p_statement.setInt(6, hero.getDefence());
            p_statement.setInt(7, hero.getHp());
            p_statement.setInt(8, hero.getId());
            p_statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error DB: " + e.getMessage());
        }
    }

    public static ArrayList<Hero> getAllHeroes() {
        String sqlQuery = "SELECT * FROM heroes";
        ArrayList<Hero> arrayList = new ArrayList<>();

        try (Statement stmt = getConnection().createStatement();
             ResultSet rs = stmt.executeQuery(sqlQuery)) {
            for (int i = 1; rs.next(); i++) {
                arrayList.add(makeHero(rs));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return arrayList;
    }
}

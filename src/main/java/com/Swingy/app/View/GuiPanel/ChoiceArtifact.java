package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Artifact;
import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Villain;
import com.Swingy.app.View.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

public class ChoiceArtifact extends JPanel {
    private JButton leaveButton = new JButton( "Leave");
    private JButton keepButton = new JButton("Keep");

    private JButton switchButton = new JButton("Switch Interface");

    private ArrayBlockingQueue<String> queue;

    public String    start(Hero hero, Artifact artifact) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(hero, artifact);
        });
        return queue.take();
    }

    private void Build(Hero hero, Artifact artifact) {
        Gui.getFrame().setTitle("Game");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        this.add(new JLabel(artifact.StrEffectOnHero(hero)), gbc);

        JPanel ChoicePannel = new JPanel();
        ChoicePannel.add(leaveButton);
        ChoicePannel.add(keepButton);
        ChoicePannel.setVisible(true);
        this.add(ChoicePannel, gbc);

        this.add(switchButton, gbc);

        Gui.getFrame().addKeyListener(new ChoiceArtifact.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();

        leaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                leaveArtifact();
            }
        });

        keepButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                keepArtifact();
            }
        });

        switchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                    e.getKeyCode() == KeyEvent.VK_K)
                keepArtifact();
            if (e.getKeyCode() == KeyEvent.VK_L)
                leaveArtifact();
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void leaveArtifact() {
        queue.offer("leave");
        this.setVisible(false);
    }

    private void keepArtifact() {
        queue.offer("keep");
        this.setVisible(false);
    }

    private void SwitchInterface() {
        queue.offer("switch");
        this.setVisible(false);
    }
}

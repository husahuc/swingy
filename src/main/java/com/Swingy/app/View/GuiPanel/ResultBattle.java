package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Map;
import com.Swingy.app.Model.Villain;
import com.Swingy.app.View.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

public class ResultBattle extends JPanel {
    private JButton continueButton = new JButton("Continue");

    private ArrayBlockingQueue<String> queue;

    public void    start(Hero hero, Villain villain, int new_xp, int new_level) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(hero, villain,new_xp, new_level);
        });
        queue.take();
    }

    private void Build(Hero hero, Villain villain, int new_xp, int new_level) {
        Gui.getFrame().setTitle("Game");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;

        JPanel ResultPanel = new JPanel();
        if (hero.IsAlive())
            ResultPanel.add(new JLabel(hero.Getname() + " have won against the villain " + villain.getName()));
        else
            ResultPanel.add(new JLabel(hero.Getname() + " lost against the villain " + villain.getName()));
        if (new_level > 0)
            ResultPanel.add(new JLabel("You have " + new_level + " new level"));
        ResultPanel.add(new JLabel(String.valueOf(new_xp) + "xp gains"));
        ResultPanel.setVisible(true);
        this.add(ResultPanel);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;

        SwingyPanels.PanelBox heroPanel = new SwingyPanels.PanelBox();
        heroPanel.Build(hero.toPairs());
        this.add(heroPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;

        SwingyPanels.PanelBox villainPanel = new SwingyPanels.PanelBox();
        villainPanel.Build(villain.toPairs());
        this.add(villainPanel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;

        JPanel ChoicePannel = new JPanel();
        ChoicePannel.add(continueButton);
        ChoicePannel.setVisible(true);
        this.add(ChoicePannel, gbc);

        Gui.getFrame().addKeyListener(new ResultBattle.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();

        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                continueGame();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
                continueGame();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void continueGame() {
        queue.offer("continue");
        this.setVisible(false);
    }
}
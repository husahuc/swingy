package com.Swingy.app.View.GuiPanel;

import javax.swing.*;
import javax.validation.constraints.Null;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

import com.Swingy.app.View.Interface;
import com.Swingy.app.View.Gui;

public class Menu extends JPanel {
    private JButton createHeroButton = new JButton("Create Hero");
    private JButton selectHeroButton = new JButton("Select Hero");
    private JButton SwitchInterfaceButton = new JButton("Switch Interface");
    private ArrayBlockingQueue<String> queue;

    public String start() throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build();
        });
        return queue.take();
    }

    private void Build() {
        Gui.getFrame().setTitle("Start");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        this.add(createHeroButton, gbc);
        this.add(selectHeroButton, gbc);
        this.add(SwitchInterfaceButton, gbc);

        Gui.getFrame().addKeyListener(new Menu.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();

        createHeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenCreateHero();
            }
        });

        selectHeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenSelectHero();
            }
        });

        SwitchInterfaceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_C)
                OpenCreateHero();
            if (e.getKeyCode() == KeyEvent.VK_S)
                OpenSelectHero();
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void OpenCreateHero() {
        queue.offer("create");
        this.setVisible(false);
    }

    private void OpenSelectHero() {
        queue.offer("select");
        this.setVisible(false);
    }

    private void SwitchInterface() {
        queue.offer("switch");
        this.setVisible(false);
    }
}
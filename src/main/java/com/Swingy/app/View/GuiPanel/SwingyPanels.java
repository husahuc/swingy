package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Map;
import javafx.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Array;

public class SwingyPanels {
    public static class PanelMap extends JPanel {
        private Map map;
        private Boolean hiddenVisible;
        private static final int SIZESQUARE = 10;

        public PanelMap(Map map, boolean hiddenVisible) {
            this.map = map;
            this.hiddenVisible = hiddenVisible;
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(SIZESQUARE * map.getSize(), SIZESQUARE * map.getSize());
        }

        @Override
        public void paintComponent(Graphics g) {

            Graphics2D g2 = (Graphics2D) g;


            for (int i = 0; i < map.getSize(); ++i) {
                for (int j = 0; j < map.getSize(); ++j) {
                    g2.drawRect(SIZESQUARE * j, SIZESQUARE * i, SIZESQUARE, SIZESQUARE);
                }
            }

            //this.setPreferredSize(new Dimension(SIZESQUARE * map.getSize(), SIZESQUARE * map.getSize()));
            
            int[][] map_array = map.getMap();
            for (int x = 0; x < map_array.length; x++) {
                for (int y = 0; y < map_array.length; y++) {
                    if (map_array[x][y] == 3 && hiddenVisible == true) {
                        g2.setColor(new Color(255, 255, 0, 75));
                        g2.fillRect(y * SIZESQUARE, x * SIZESQUARE, SIZESQUARE, SIZESQUARE);
                    }
                    if (map_array[x][y] == 1) {
                        g2.setColor(new Color(128, 255, 0, 75));
                        g2.fillRect(y * SIZESQUARE, x * SIZESQUARE, SIZESQUARE, SIZESQUARE);
                    }
                    if (map_array[x][y] == 2 && hiddenVisible == true) {
                        g2.setColor(new Color(255, 0, 0, 75));
                        g2.fillRect(y * SIZESQUARE, x * SIZESQUARE, SIZESQUARE, SIZESQUARE);
                    }
                }
            }
        }
    }

    public static class PanelBox extends JPanel {
        public void Build(Pair<String, String>[] pairs) {
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

            JLabel labelText;

            for (int i = 0; i < pairs.length; i++) {
                labelText = new JLabel(pairs[i].getKey() + ": " + pairs[i].getValue());
                this.add(labelText);
            }
            this.setVisible(true);
        }
    }
}

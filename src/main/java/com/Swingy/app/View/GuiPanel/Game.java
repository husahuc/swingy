package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Map;
import com.Swingy.app.View.Console;
import com.Swingy.app.View.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Game extends JPanel {
    private JButton buttonNorth = new JButton("North");
    private JButton buttonSouth = new JButton("South");
    private JButton buttonEast = new JButton("East");
    private JButton buttonWest = new JButton("West");

    private JButton switchButton = new JButton("Switch Interface");

    private ArrayBlockingQueue<String> queue;
    private KeyAdapter keyAdapter;

    public String start(Hero hero, Map map, String log) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(hero, map, log);
        });
        return queue.take();
    }

    private void Build(Hero hero, Map map, String log) {
        Gui.getFrame().setTitle("Game");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;

        SwingyPanels.PanelBox heroPanel = new SwingyPanels.PanelBox();
        heroPanel.Build(hero.toPairs());
        this.add(heroPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        SwingyPanels.PanelMap mapPanel = new SwingyPanels.PanelMap(map, false);
        mapPanel.setVisible(true);
        this.add(mapPanel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;

        JPanel SelectDirectionPanel = new JPanel();
        SelectDirectionPanel.setLayout(new BorderLayout());
        SelectDirectionPanel.add(buttonNorth,BorderLayout.NORTH);
        SelectDirectionPanel.add(buttonSouth, BorderLayout.SOUTH);
        SelectDirectionPanel.add(buttonEast, BorderLayout.EAST);
        SelectDirectionPanel.add(buttonWest, BorderLayout.WEST);
        SelectDirectionPanel.setVisible(true);
        this.add(SelectDirectionPanel, gbc);

        gbc.gridy = 2;
        this.add(switchButton, gbc);

        Gui.getFrame().addKeyListener(new SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.showFrame();

        buttonNorth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                movedirection("north");
            }
        });

        buttonSouth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                movedirection("south");
            }
        });

        buttonEast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                movedirection("east");
            }
        });

        buttonWest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                movedirection("west");
            }
        });

        switchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_UP)
                movedirection("north");
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                movedirection("south");
            if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                movedirection("east");
            if (e.getKeyCode() == KeyEvent.VK_LEFT)
                movedirection("west");
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void movedirection(String direction) {
        queue.offer(direction);
        this.setVisible(false);
    }

    private void SwitchInterface() {
        queue.offer("switch");
        this.setVisible(false);
    }
}

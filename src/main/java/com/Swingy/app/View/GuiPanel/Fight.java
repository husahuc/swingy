package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Villain;
import com.Swingy.app.View.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

public class Fight extends JPanel {
    private JButton runButton = new JButton("Run away");
    private JButton fightButton = new JButton("Fight!");

    private JButton switchButton = new JButton("Switch Interface");

    private ArrayBlockingQueue<String> queue;

    public String    start(Hero hero, Villain villain) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(hero, villain);
        });
        return queue.take();
    }

    private void Build(Hero hero, Villain villain) {
        Gui.getFrame().setTitle("Game");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 0;
        gbc.gridy = 0;

        SwingyPanels.PanelBox heroPanel = new SwingyPanels.PanelBox();
        heroPanel.Build(hero.toPairs());
        this.add(heroPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        SwingyPanels.PanelBox villainPanel = new SwingyPanels.PanelBox();
        villainPanel.Build(villain.toPairs());
        this.add(villainPanel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;

        JPanel ChoicePannel = new JPanel();
        ChoicePannel.add(runButton);
        ChoicePannel.add(fightButton);
        ChoicePannel.setVisible(true);
        this.add(ChoicePannel, gbc);

        gbc.gridy = 2;

        this.add(switchButton, gbc);

        Gui.getFrame().addKeyListener(new Fight.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();

        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                run();
            }
        });

        fightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fight();
            }
        });

        switchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                e.getKeyCode() == KeyEvent.VK_F)
                fight();
            if (e.getKeyCode() == KeyEvent.VK_R)
                run();
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void run() {
        queue.offer("run");
        this.setVisible(false);
    }

    private void fight() {
        queue.offer("fight");
        this.setVisible(false);
    }

    private void SwitchInterface() {
        queue.offer("switch");
        this.setVisible(false);
    }
}

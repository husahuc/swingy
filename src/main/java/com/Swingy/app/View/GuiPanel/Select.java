package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.HeroClass;
import com.Swingy.app.View.Gui;
import javafx.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class Select extends JPanel {
    private JList heroListField = new JList();
    private JLabel heroesLabel = new JLabel("Heroes");
    private JButton selectHeroButton = new JButton("Select Hero");
    private JButton createHeroButton = new JButton("Create a new Hero");
    private JButton switchButton = new JButton("Switch Interface");

    private String choice = "";
    private Hero hero = null;
    private ArrayBlockingQueue<String> queue;

    public Pair<Hero, String> start(ArrayList<Hero> heroes) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(heroes);
        });
        queue.take();
        return new Pair<Hero, String>(hero, choice);
    }

    private void Build(ArrayList<Hero> heroes) {
        Gui.getFrame().setTitle("Select");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        JPanel heroesPanel = new JPanel();
        if (heroes.isEmpty())
            heroesPanel.add(new JLabel("No heroes found"));
        else {
            heroesPanel.add(heroesLabel);
            heroListField.setListData(heroes.toArray());
            heroListField.setSelectedIndex(0);
            heroesPanel.add(heroListField);
            heroesPanel.add(selectHeroButton, gbc);
        }
        heroesPanel.setVisible(true);
        this.add(heroesPanel, gbc);

        this.add(createHeroButton, gbc);
        this.add(switchButton, gbc);

        Gui.getFrame().addKeyListener(new Select.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();


        selectHeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ValidateForm();
            }
        });

        createHeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenCreateHero();
            }
        });

        switchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
                ValidateForm();
            if (e.getKeyCode() == KeyEvent.VK_C)
                OpenCreateHero();
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void ValidateForm() {
        this.hero = (Hero) heroListField.getSelectedValue();
        queue.offer("");
        this.setVisible(false);
    }

    private void OpenCreateHero() {
        this.choice = "create";
        queue.offer(this.choice);
        this.setVisible(false);
    }

    private void SwitchInterface() {
        this.choice = "switch";
        queue.offer(this.choice);
        this.setVisible(false);
    }
}

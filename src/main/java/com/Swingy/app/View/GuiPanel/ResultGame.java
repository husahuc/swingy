package com.Swingy.app.View.GuiPanel;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.Map;
import com.Swingy.app.Model.Villain;
import com.Swingy.app.View.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

public class ResultGame extends JPanel{
    private JButton continueButton = new JButton("Continue to a new game");
    private JButton quitButton = new JButton("Quit");

    private JButton switchButton = new JButton("Switch Interface");

    private ArrayBlockingQueue<String> queue;

    public String    start(Hero hero, Map map) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(hero, map);
        });
        return queue.take();
    }

    private void Build(Hero hero, Map map) {
        Gui.getFrame().setTitle("Game");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;

        SwingyPanels.PanelBox heroPanel = new SwingyPanels.PanelBox();
        heroPanel.Build(hero.toPairs());
        this.add(heroPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        SwingyPanels.PanelMap mapPanel = new SwingyPanels.PanelMap(map, true);
        mapPanel.setVisible(true);
        this.add(mapPanel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;

        JPanel ChoicePannel = new JPanel();
        ChoicePannel.add(continueButton);
        ChoicePannel.add(quitButton);
        ChoicePannel.setVisible(true);
        this.add(ChoicePannel, gbc);

        gbc.gridy = 2;
        this.add(switchButton, gbc);

        Gui.getFrame().addKeyListener(new ResultGame.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();

        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                continueGame();
            }
        });

        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quitGame();
            }
        });

        switchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwitchInterface();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                    e.getKeyCode() == KeyEvent.VK_C)
                continueGame();
            if (e.getKeyCode() == KeyEvent.VK_Q)
                quitGame();
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                SwitchInterface();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void continueGame() {
        queue.offer("continue");
        this.setVisible(false);
    }

    private void quitGame() {
        queue.offer("quit");
        this.setVisible(false);
    }

    private void SwitchInterface() {
        queue.offer("switch");
        this.setVisible(false);
    }
}

package com.Swingy.app.View.GuiPanel;

import javax.swing.*;
import javax.validation.constraints.Null;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;

import com.Swingy.app.Model.Hero;
import com.Swingy.app.Model.HeroClass;
import com.Swingy.app.View.Interface;
import com.Swingy.app.View.Gui;

public class FormCreate extends JPanel{

    private JLabel heroNameLabel = new JLabel("Hero name:");
    private JTextField heroNameField = new JTextField(10);

    private JLabel classesLabel = new JLabel("Classes");
    private JList classesListField = new JList();

    private JButton createHeroButton = new JButton("Create Hero");

    private Hero.CreateBuilder heroBuilder;
    private ArrayBlockingQueue<Hero.CreateBuilder> queue;

    public Hero.CreateBuilder start(HeroClass[] classes, String error) throws InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Can't be called from EDT.");
        }
        queue = new ArrayBlockingQueue<>(1);
        SwingUtilities.invokeLater(() -> {
            Build(classes, error);
        });
        return queue.take();
    }

    private void Build(HeroClass[] classes, String error) {
        Gui.getFrame().setTitle("Create");
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        if (error != "")
            this.add(new JLabel("error:" + error), gbc);

        JPanel createHeroPanel = new JPanel();
        createHeroPanel.add(heroNameLabel);
        createHeroPanel.add(heroNameField);
        createHeroPanel.setVisible(true);
        this.add(createHeroPanel, gbc);

        JPanel classesPanel = new JPanel();
        classesPanel.add(classesLabel);
        classesListField.setListData(classes);
        classesListField.setSelectedIndex(0);
        classesPanel.add(classesListField);
        classesPanel.setVisible(true);
        this.add(classesPanel, gbc);

        this.add(createHeroButton, gbc);

        Gui.getFrame().addKeyListener(new FormCreate.SwingyKeyListener());
        this.setVisible(true);
        Gui.getFrame().setContentPane(this);
        Gui.getFrame().revalidate();
        Gui.showFrame();


        createHeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ValidateForm();
            }
        });
    }

    public class SwingyKeyListener implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
                ValidateForm();
        }
        public void keyTyped(KeyEvent e) { }
        public void keyReleased(KeyEvent e) { }
    }

    private void ValidateForm() {

        heroBuilder = new Hero.CreateBuilder();

        heroBuilder.WithName(heroNameField.getText());
        heroBuilder.WithClass((HeroClass) classesListField.getSelectedValue());

        queue.offer(heroBuilder);
        this.setVisible(false);
    }
}

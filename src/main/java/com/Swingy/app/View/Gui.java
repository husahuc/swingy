package com.Swingy.app.View;
import com.Swingy.app.Model.*;
import com.Swingy.app.View.GuiPanel.*;
import javafx.util.Pair;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.*;
import javax.validation.constraints.Null;

public class Gui implements Interface {
    private static JFrame frame;

    @Override
    public String    StartMenu() {
        Menu menu = new Menu();

        try {
            return menu.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    public Pair<Hero, String> SelectHero(ArrayList<Hero> heroes)
    {
        Select form = new Select();
        try {
            return form.start(heroes);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Hero.CreateBuilder CreateHero(HeroClass[] classes, String error)
    {
        FormCreate form = new FormCreate();
        try {
            return form.start(classes, error);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String    Game(Hero hero, Map map, String log)
    {
        Game form = new Game();
        try {
            return form.start(hero, map, log);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String    Fight(Hero hero, Villain villain) {
        Fight form = new Fight();
        try {
            return form.start(hero, villain);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String    ChoiceArtifact(Hero hero, Artifact artifact) {
        ChoiceArtifact form = new ChoiceArtifact();
        try {
            return form.start(hero, artifact);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void      ResultBattle(Hero hero, Hero old_hero, Villain villain ) {
        int new_xp = hero.getExperience() - old_hero.getExperience();
        int new_level = hero.getLevel() - old_hero.getLevel();

        ResultBattle form = new ResultBattle();
        try {
            form.start(hero, villain, new_xp, new_level);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String    ResultGame(Hero hero, Map map) {
        ResultGame form = new ResultGame();
        try {
            return form.start(hero, map);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static JFrame getFrame() {
        if (frame == null) {
            frame = new JFrame();
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setSize(800, 700);
            frame.setFocusable(true);
        }
        return frame;
    }

    public static void showFrame() {
        if (frame != null)
            frame.setVisible(true);
    }

    public static void hideFrame() {
        if (frame != null) {
            frame.setVisible(false);
            frame.dispose();
        }
    }

    public void      CloseInterface() {
        hideFrame();
    }
}

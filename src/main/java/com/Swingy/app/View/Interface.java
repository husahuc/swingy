package com.Swingy.app.View;
import com.Swingy.app.Model.*;

import java.util.ArrayList;

import javafx.util.Pair;

public interface Interface {
    public String   StartMenu();
    public Hero.CreateBuilder      CreateHero(HeroClass[] classes, String error);
    public Pair<Hero, String> SelectHero(ArrayList<Hero> heroes);
    public String    Game(Hero hero, Map map, String log);
    public String    Fight(Hero hero, Villain villain);
    public String    ChoiceArtifact(Hero hero, Artifact artifact);
    public void      ResultBattle(Hero hero, Hero old_hero, Villain villain );
    public String    ResultGame(Hero hero, Map map);
    public void      CloseInterface();
}
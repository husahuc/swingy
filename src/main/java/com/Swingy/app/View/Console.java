package com.Swingy.app.View;
import com.Swingy.app.Model.*;
import com.Swingy.app.View.GuiPanel.FormCreate;
import com.Swingy.app.View.Interface;
import com.Swingy.app.utils.DataBase;
import javafx.util.Pair;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;

public class Console implements Interface {
    private static Scanner     scanner_interface = null;

    public String    StartMenu() {
        String choice = "";
        String[] choices = {"create", "select" , "switch"};

        while (! Arrays.asList(choices).contains(choice)) {
            System.out.print("Start menu: ");
            for (String choice_select : choices ) {
                System.out.print(choice_select+' ');
            }
            System.out.println();
            choice = getScanner().nextLine();
        }
        return choice;
    }

    public Hero.CreateBuilder CreateHero(HeroClass[] classes, String error) {
        if (error != "")
            System.err.println("ERROR:"+error);
        Hero.CreateBuilder buildHero = new Hero.CreateBuilder();
        System.out.println("create hero");
        System.out.println("hero name:");
        String HeroName = "";
        System.out.flush();
        HeroName = getScanner().nextLine();
        buildHero.WithName(HeroName);

        ArrayList<String> ClassnameList = new ArrayList<String>();
        ArrayList<HeroClass> classesArray = new ArrayList<>(Arrays.asList(classes));
        String ClassName = "";

        while (! ClassnameList.contains(ClassName) && ClassName == "") {
            System.out.println("hero class select in options:");
            int i = 0;
            for (HeroClass _class : classes) {
                System.out.println(_class.toString());
                ClassnameList.add(_class.GetName());
                i++;
            }
            ClassName = getScanner().nextLine();
        }

        HeroClass heroClass = HeroClass.HeroClassFactory.getClassByName(ClassName);

        buildHero.WithClass(heroClass);

        return buildHero;
    }

    public Pair<Hero, String> SelectHero(ArrayList<Hero> heroes)
    {
        int id_select;
        Hero hero = null;
        String input_text = "";
        if (heroes.isEmpty()) {
            System.out.println("No heroes fond, redirect to create hero");
            return new Pair<Hero, String>(null, "create");
        }

        while (hero == null && !input_text.equals("create") && !input_text.equals("switch")) {
            for (int i = 0; i < heroes.size() ; i++)
            {
                System.out.print("number: " + i + " ");
                System.out.println(heroes.get(i).toString());
            }
            System.out.println("Select hero number in options, or create, or switch:");
            input_text = getScanner().nextLine();
            try {
                id_select = Integer.parseInt(input_text);
                if (id_select <= heroes.size() && id_select >= 0) {
                    hero = heroes.get(id_select);
                }
            }
            catch (NumberFormatException e) {
            }
        }

        return new Pair<Hero, String>(hero, input_text);
    }

    public String    Game(Hero hero, Map map, String log)
    {
        System.out.println(hero.toString());
        String choice = "";
        String choices[] = {"north", "south", "east", "west", "switch"};
        this.PrintMap(map, false);
        while (! Arrays.asList(choices).contains(choice)) {
            System.out.print("Game: ");
            for (String choice_select : choices ) {
                System.out.print(choice_select + ' ');
            }
            System.out.println("");
            choice = getScanner().nextLine();
        }
        return choice;
    }

    public String    Fight(Hero hero, Villain villain) {
        System.out.println(hero.toString());
        System.out.println(villain.toString());
        String choice = "";
        String choices[] = {"fight", "run", "switch"};
        while (! Arrays.asList(choices).contains(choice)) {
            System.out.print("Fight : ");
            for (String choice_select : choices ) {
                System.out.print(choice_select + ' ');
            }
            System.out.println("");
            choice = getScanner().nextLine();
        }
        return choice;
    }

    public String    ChoiceArtifact(Hero hero, Artifact artifact) {
        String choices[] = {"leave", "keep", "switch"};
        String choice = "";
        System.out.println(artifact.StrEffectOnHero(hero));
        while (! Arrays.asList(choices).contains(choice)) {
            System.out.print("Keep the artifact hoices: ");
            for (String choice_select : choices ) {
                System.out.print(choice_select + ' ');
            }
            System.out.println("");
            choice = getScanner().nextLine();
        }
        return choice;
    }

    public void      ResultBattle(Hero hero, Hero old_hero, Villain villain ) {
        int new_xp = hero.getExperience() - old_hero.getExperience();
        int new_level = hero.getLevel() - old_hero.getLevel();
        if (new_level > 0)
            System.out.println("You have " + new_level + " new level");
        System.out.println(String.valueOf(new_xp) + "xp gains");
        System.out.println(hero.toString());
        if (hero.IsAlive())
            System.out.println(hero.Getname() + " have won against the villain " + villain.getName());
        else
            System.out.println(hero.Getname() + " lost against the villain " + villain.getName());
    }

    public String    ResultGame(Hero hero, Map map) {
        String choice = "";
        String choices[] = {"continue", "quit", "switch"};
        System.out.println(hero.toString());
        PrintMap(map, true);
        while (! Arrays.asList(choices).contains(choice)) {
            System.out.print("choices : ");
            for (String choice_select : choices ) {
                System.out.print(choice_select + ' ');
            }
            System.out.println("");
            choice = getScanner().nextLine();
        }
        return choice;
    }

    public void    PrintMap(Map map, boolean hiddenVisible) {
        int[][] map_array = map.getMap();
        for (int x = 0; x < map_array.length; x++)
        {
            for (int y = 0; y < map_array.length; y++)
            {
                if (map_array[x][y] == 3 && hiddenVisible == true)
                    System.out.print("·");
                else if (map_array[x][y] == 1)
                    System.out.print("x");
                else if (map_array[x][y] == 2 && hiddenVisible == true)
                    System.out.print("o");
                else
                    System.out.print(" ");
            }
            System.out.println("|");
        }
    }

    public static Scanner getScanner() {
        if (scanner_interface == null)
            scanner_interface = new Scanner(System.in);
        return scanner_interface;
    }

    public void      CloseInterface() {
        scanner_interface = null;
    }
}

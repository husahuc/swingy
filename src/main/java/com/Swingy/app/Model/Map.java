package com.Swingy.app.Model;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Map {
    int[][] _map;

    public Map(int level) {
        int size = (level - 1) * 5 + 10 - (level % 2);
        _map = new int[size][size];
    }

    public ArrayList<Villain> PopulateVillain(int level, Random rand) {
        ArrayList<Villain> ListVillain = new ArrayList<>();

        int num_vilains = this.getLenght() + 1 + rand.nextInt(this.getLenght());

        while (num_vilains > 0) {
            Point p_rand = new Point(rand.nextInt(this.getLenght()), rand.nextInt(this.getLenght()));
            int position = this.getPosition(p_rand);
            if (position == 0)
            {
                this.setPosition(p_rand, 2);
                Villain.VillainFactory factory = new Villain.VillainFactory();
                Villain villain = factory.getVillain(level, rand);
                villain.setPosition(p_rand);
                ListVillain.add(villain);
                num_vilains--;
            }
        }

        return ListVillain;
    }

    // getters
    public int[][] getMap() {
        return _map;
    }
    public int getPosition(Point p) {
        if (p.x >= _map.length || p.x < 0 ||
            p.y >= _map.length || p.y < 0) {
            return -1;
        }
        return _map[p.x][p.y];
    }
    public int getLenght() { return  _map.length - 1; }
    public int getSize() { return  _map.length; }

    // setters
    public Point getPositionCenter()
    {
        return new Point((_map.length/2), (_map.length/2));
    }
    public void setPosition(Point p, int value) {
        if (p.x < _map.length && p.x >= 0 &&
                p.y < _map.length && p.y >= 0) {
            _map[p.x][p.y] = value;
        }
    }

}

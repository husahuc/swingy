package com.Swingy.app.Model;
import com.Swingy.app.utils.DataBase;
import javafx.util.Pair;
import org.codehaus.plexus.interpolation.PrefixAwareRecursionInterceptor;

import java.sql.*;

import javax.lang.model.type.NullType;
import javax.validation.constraints.*;

public class Hero {
    private String name;

    private String _class;
    private int level;
    private int experience;
    private int attack;
    private int defence;
    private int hp;
    private int id;

    private static final String FILE = "Hero_save";

    // Builder in steps.
    public static class CreateBuilder {
        @NotNull
        @Size(min = 3, max = 10)
        private String name;
        private HeroClass _class;

        public CreateBuilder() {}

        public CreateBuilder WithName(String name) {
            this.name = name;
            return this;
        }

        public CreateBuilder WithClass(HeroClass _class) {
            this._class = _class;
            return this;
        }

        public HeroClass get_Class() {return this._class;}
        public String getName() {return this.name;}

        public Hero build() {
            Hero Hero = new Hero();
            Hero.name = this.name;

            Hero._class = this._class.GetName();
            Hero.attack = this._class.GetAttack();
            Hero.defence = this._class.GetDefence();
            Hero.hp = this._class.GetHp();
            Hero.experience = 0;
            Hero.level = 0;
            return Hero;
        }
    }

    public static class DatabaseBuilder {
        private String name;
        private String class_name;
        private int level;
        private int xp;
        private int attack;
        private int defence;
        private int hp;

        public DatabaseBuilder(String name, String class_name, int level, int xp, int attack, int defence, int hp) {
            this.name = name;
            this.class_name = class_name;
            this.level = level;
            this.xp = xp;
            this.attack = attack;
            this.defence = defence;
            this.hp = hp;
        }

        public Hero build() {
            Hero hero = new Hero();
            hero.name = this.name;
            hero._class = this.class_name;
            hero.level = this.level;
            hero.experience = this.xp;
            hero.attack = this.attack;
            hero.defence = this.defence;
            hero.hp = this.hp;
            return hero;
        }
    }

    public boolean IsAlive() {
        if (this.hp > 0)
            return true;
        else
            return false;
    }

    public void ApplyArtifact(Artifact artifact) {
        if (artifact.getType().equals(artifact.types[0]))
            this.AddAttack(artifact.getValue());
        else if (artifact.getType().equals(artifact.types[1]))
            this.AddDefence(artifact.getValue());
        else if (artifact.getType().equals(artifact.types[2]))
            this.AddHp(artifact.getValue());
    }

    private Hero() {}

    // Getters
    public String Getname() { return name; }
    public String Get_Class() { return _class; }
    public int getLevel() { return  level; }
    public int getExperience() { return experience; }
    public int getAttack() { return attack; }
    public int getDefence() { return defence; }
    public int getHp() { return hp; }
    public int getId() { return id; }

    // Setters
    public void SetName(String name) { this.name = name; }
    public void SetId(int id) {this.id = id; }
    public void setExperience(int experience) {this.experience = experience;}
    public void setLevel(int level) {this.level = level;}
    public void setHp(int hp) {this.hp = hp;}
    public void setId(int id) {this.id = id;}
    public void setAttack(int attack) {this.attack = attack;}
    public void setDefence(int defence) {this.defence = defence;}

    public void AddLevel(int add) { this.level += add; }
    public void AddExperience(int add) { this.experience += add; }
    public void AddAttack(int add) { this.attack += add; }
    public void AddDefence(int add) { this.defence += add; }
    public void AddHp(int add) { this.hp += add; }

    @Override
    public Hero clone() {
        Hero hero = new Hero();
        hero.name = this.name;
        hero._class = this._class;
        hero.level = this.level;
        hero.experience = this.experience;
        hero.attack = this.attack;
        hero.defence = this.defence;
        hero.hp = this.hp;
        return hero;
    }

    @Override
    public String toString() {
        return
                "HERO :"+ System.lineSeparator() +"name:" + name + "" +
                        ", class:" + _class +
                        ", level:" + level +
                        ", xp:" + experience +
                        ", attack:" + attack +
                        ", defence:" + defence +
                        ", hp:" + hp;
    }

    public Pair<String, String>[] toPairs() {
        Pair<String, String>[] pairs = new Pair[7];
        pairs[0] = new Pair<>("name", name);
        pairs[1] = new Pair<>("class", _class);
        pairs[2] = new Pair<>("level", String.valueOf(level));
        pairs[3] = new Pair<>("xp", String.valueOf(experience));
        pairs[4] = new Pair<>("attack", String.valueOf(attack));
        pairs[5] = new Pair<>("defence", String.valueOf(defence));
        pairs[6] = new Pair<>("hp", String.valueOf(hp));
        return pairs;
    }
}
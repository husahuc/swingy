package com.Swingy.app.Model;

import com.github.javafaker.Faker;
import javafx.util.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Villain {
    private String name;
    private int hp;
    private int defence;
    private int attack;
    private Point position;

    // Getters
    public int getAttack() {return attack;}
    public int getDefence() {return defence;}
    public int getHp() {return hp;}
    public Point getPosition() {return position;}
    public String getName() {return name;}

    // Setters
    public void setAttack(int attack) {this.attack = attack;}
    public void setDefence(int defence) {this.defence = defence;}
    public void setHp(int hp) {this.hp = hp;}
    public void setName(String name) {this.name = name;}
    public void setPosition(Point position) {this.position = position;}

    public void removeHp(int value) {this.hp -= value;}

    public static class VillainFactory {
        public Villain getVillain(int level, Random rand) {
            Villain villain = new Villain();
            Faker faker = new Faker();
            int coff = (level / 2) + 1;
            villain.setAttack((rand.nextInt(10) + 2) * coff);
            villain.setDefence(rand.nextInt(5) * coff);
            villain.setHp(rand.nextInt(90) + 20 * coff);
            villain.setName(faker.name().firstName());
            return villain;
        }
    }

    public static Villain GetVillainInArray(Point p, ArrayList<Villain> villains) {
        for (int i = 0; i < villains.size(); i++) {
            if (villains.get(i).getPosition().equals(p))
                return villains.get(i);
        }
        return null;
    }

    @Override
    public String toString() {
        return "VILLAIN :"+ System.lineSeparator() +"name:" + name +
                ", hp:" + hp +
                ", attack:" + attack +
                ", defence:" + defence;
    }

    public Pair<String, String>[] toPairs() {
        Pair<String, String>[] pairs = new Pair[4];
        pairs[0] = new Pair<>("name", name);
        pairs[1] = new Pair<>("hp", String.valueOf(hp));
        pairs[2] = new Pair<>("attack", String.valueOf(attack));
        pairs[3] = new Pair<>("defence", String.valueOf(defence));
        return pairs;
    }
}

package com.Swingy.app.Model;

public class HeroClass {
    String name;
    int     attack;
    int     defence;
    int     hp;

    public HeroClass(String name, int attack, int defence, int hp) {
        this.name = name;
        this.attack = attack;
        this.defence = defence;
        this.hp = hp;
    }

    @Override
    public String toString() {
        return
                "'" + name + '\'' +
                ", attack=" + attack +
                ", defence=" + defence +
                ", hp=" + hp;
    }

    public String GetName() { return (this.name);}
    public int GetAttack() { return (this.attack);}
    public int GetDefence() { return (this.defence);}
    public int GetHp() { return (this.hp);}

    public static class HeroClassFactory {

        public static HeroClass[] getClasses() {
            HeroClass[] ClassesArray;
            ClassesArray = new HeroClass[3];

            ClassesArray[0] = new HeroClass("Paladin", 50, 50, 90);
            ClassesArray[1] = new HeroClass("Warrior", 70, 30, 80);
            ClassesArray[2] = new HeroClass("Student", 20, 20, 120);

            return ClassesArray;
        }

        public static HeroClass getClassByName(String name) {
            HeroClass ret = null;
            HeroClass[] classes = getClasses();
            for (HeroClass _class : classes) {
                if (_class.GetName().equals(name))
                    ret = _class;
            }
            return  (ret);
        }
    }
}

package com.Swingy.app.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Artifact {
    private String type;
    private int value;

    public static final String[] types = new String[]{"Weapon", "Armor", "Helm"};

    public Artifact(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public String StrEffectOnHero(Hero hero) {
        String str;
        if (type == types[0])
            str = "Artifact Weapon attack:+" + this.value + " new hero attack with:" + (hero.getAttack() + this.value);
        else if (type == types[1])
            str = "Artifact Armor defence:+" + this.value + " new hero defence with:" + (hero.getDefence() + this.value);
        else
            str = "Artifact Helm hp:+" + this.value + " new hero hp with:" + (hero.getHp() + this.value);

        return str;
    }

    // getters
    public int getValue() {return value;}
    public String getType() {return type;}

    public static class ArtifactFatory {
        public Artifact getArtifact(Villain villain, Random rand) {
            String type = types[rand.nextInt(types.length)];
            int value;
            if (type == types[0])
                value = villain.getAttack();
            else if (type == types[1])
                value = villain.getDefence();
            else
                value = villain.getHp();
            return new Artifact(type, value);
        }
    }
}
package com.Swingy.app;

import com.Swingy.app.Controler.GameLoop;
import com.Swingy.app.View.Console;
import com.Swingy.app.View.Gui;
import com.Swingy.app.View.Interface;


public class Main {

    public static void main(String[] args) {
        GameLoop GameLoop;
        if (args.length != 1) {
            System.out.println("usage:\n programm console | gui");
            return;
        }
        if (args[0].equals("console"))
        {
            Interface ConsoleInterface = new Console();
            GameLoop = new GameLoop(ConsoleInterface);
        }
        else if (args[0].equals("gui"))
        {
            Interface GuiInterface = new Gui();
            GameLoop = new GameLoop(GuiInterface);
        }
        else {
            System.out.println("usage:\n programm console | gui");
            return;
        }
        GameLoop.Start();
    }
}
